cmake_minimum_required(VERSION 3.0.0 FATAL_ERROR)	

project(client VERSION 0.1.0)

add_executable(client ../client.c)